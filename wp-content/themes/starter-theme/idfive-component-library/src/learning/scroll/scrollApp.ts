export default class ScrollApp {
    protected containerElement: HTMLElement;
    protected circleElements: NodeListOf<HTMLElement>;
    protected scrollPosition: number;
    protected circleArray: Array<HTMLElement> = [];
    protected rotateFactor: any;
    protected dynamicHeight: number;
    protected styleObj: Object;
    protected proportionsArray: Array<object> = [];
    protected sections: NodeListOf<HTMLElement>;
    constructor(containerElement, circleElements, sections) {
        this.containerElement = containerElement;
        this.circleElements = circleElements;
        this.sections = sections;
        this.rotateFactor = {
            oldValue: 0,
            currentValue: 0,
        }
        this.createItems();
        this.events();
        //this returnarray line does nothing, right?
        this.returnArray();
        this.setArray();
    }
    scrolling() {
        this.scrollPosition = window.pageYOffset;
        this.expandItems();
        this.rotateContainer();
        this.validateSectionScrolll();
    }
    createItems() {
        for (let i = 0; i < this.circleElements.length; i++) {
            this.circleArray.push(this.circleElements[i]);
        }
    }
    rotateContainer() {
        if (this.scrollPosition > document.body.clientHeight / (this.sections.length * 2)) {
            if (this.rotateFactor.oldValue < this.scrollPosition) {
                this.rotateFactor.currentValue += (this.dynamicHeight * 7) / 1000 + 1;
            } else {
                this.rotateFactor.currentValue -= (this.dynamicHeight * 7) / 1000 + 1;
            }
            this.containerElement.style.transform = 'translateX(-50%) translateY(-50%)' + `rotate(${this.rotateFactor.currentValue}deg)`
            this.rotateFactor.oldValue = this.scrollPosition;
        }
    }
    expandItems() {
        let maxAngle: number = Math.PI * 2;
        let angleDistance: number = maxAngle / this.circleArray.length;
        let radius: number = this.scrollPosition / 20;
        for (let i = 0; i < this.circleArray.length; i++) {
            this.circleArray[i].style.left = radius * Math.cos(i * angleDistance) + (this.containerElement.clientWidth / 2) + 'px';
            this.circleArray[i].style.top = radius * Math.sin(i * angleDistance) + (this.containerElement.clientHeight / 2) + 'px';
        }
    }
    validateSectionScrolll() {
        for (let i = 0; i < this.sections.length; i++) {
            if(this.scrollPosition + this.proportionsArray[i]['height'] / 2 > this.proportionsArray[i]['offsetTop']) {
                this.dynamicHeight = this.proportionsArray[i]['height'];
                this.updateStyles(i);
            }
        }
        this.setObj();
        console.log('Dynamic Height:', this.dynamicHeight);
    }
    updateStyles(i) {
        this.circleArray[i].style.height = this.circleArray[i]['size'] + 'px';
        this.circleArray[i].style.width = this.circleArray[i]['size'] + 'px';
    }
    returnArray() {
        let proportionsArray = []
        for (let i = 0; i < this.sections.length; i++) {
            let proportionsObj = {
                height: this.sections[i].clientHeight,
                offsetTop: this.sections[i].offsetTop,
            }
            proportionsArray.push(proportionsObj); 
        }
        return proportionsArray;
    }
    setObj() {
        for (let i = 0; i < this.circleArray.length; i++) {
            this.styleObj = {
                size: this.sections[i].clientHeight * 2 / 1000,
                color: {
                    r: 100,
                    g: 100,
                    b: 100,
                }
            }   
        }
    }
    setArray() {
        this.proportionsArray = this.returnArray();
    }
    reScale() {
        let debouncer;
        clearTimeout(debouncer)
        debouncer = setTimeout(() => {
            this.containerElement.style.left = window.innerWidth / 2 + 'px';
            this.containerElement.style.top = window.innerHeight / 2 + 'px';
        }, 200)
    }
    events() {
        window.addEventListener('scroll', event => this.scrolling())
        window.addEventListener('resize', event => this.reScale());
    }
}