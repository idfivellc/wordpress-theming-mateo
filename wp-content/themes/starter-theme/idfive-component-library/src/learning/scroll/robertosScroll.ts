import CircleElement from './items';
export default class RobertosScroll {
    protected containerElement: HTMLElement;
    protected circleElements: NodeListOf<HTMLElement>;
    protected sections: NodeListOf<HTMLElement>;
    protected circleArray: Array<any> = [];
    protected scrollPosition: number;
    protected rotateFactor: any;
    protected ticking: boolean;
    protected sectionIndex: number;
    constructor(containerElement, circleElements, sections) {
        this.containerElement = containerElement;
        this.circleElements = circleElements;
        this.sections = sections;
        this.rotateFactor = {
            oldValue: 0,
            currentValue: 0,
        }
        this.setCircleArray();
        this.events();
    }
    updateAnimation() {
        this.validateSectionScrolll();
        this.ticking = false;
    }
    onScroll() {
        this.scrollPosition = window.pageYOffset;
        this.requestTicking();
    }
    requestTicking() {
        if (!this.ticking) {
            window.requestAnimationFrame(() => {
                this.updateAnimation()
            })
        }
        this.ticking = true;
    }
    rotateContainer() {
        this.rotateFactor.currentValue = this.mathMapping(this.scrollPosition, this.sections[this.sectionIndex].offsetTop, this.sections[this.sectionIndex].offsetTop + this.sections[this.sectionIndex].offsetHeight, 0, 360); 
        this.containerElement.style.transform = 'translateX(-50%) translateY(-50%)' + `rotate(${this.rotateFactor.currentValue}deg)`
    }
    expandItems() {
        let angleDistance: number = (Math.PI * 2) / this.circleArray.length;
        let radius: number = this.scrollPosition / 10;
        for (let i = 0; i < this.circleArray.length; i++) {
            this.circleArray[i].xPos = radius * Math.cos(i * angleDistance) + (this.containerElement.clientHeight / 2),
            this.circleArray[i].yPos = radius * Math.sin(i * angleDistance) + (this.containerElement.clientHeight / 2),
            this.circleArray[i].positioning();
        }
    }
    growth() {
        let growthSize = this.mathMapping(this.scrollPosition, this.sections[this.sectionIndex].offsetTop, this.sections[this.sectionIndex].offsetTop + this.sections[this.sectionIndex].offsetHeight, 50, 180);
        for (let i = 0; i < this.circleArray.length; i++) {
            this.circleArray[i].element.style.width =  growthSize + 'px';
            this.circleArray[i].element.style.height = growthSize + 'px';
            this.circleArray[i].element.style.opacity = this.mathMapping(this.scrollPosition, this.sections[this.sectionIndex].offsetTop, this.sections[this.sectionIndex].offsetTop + this.sections[this.sectionIndex].offsetHeight, 1, 0.2);
        }
    }
    validateSectionScrolll() {
        this.interactions(this.sectionIndex);
        this.sectionIndex = this.returnSectionIndex();
    }
    interactions(i) {
        switch(i) {
            case 0: 
                this.expandItems();
            break;
            case 1:
                this.rotateContainer();
            break;
            case 2: 
                this.growth();
            break;
            case 3: 
                this.rotateContainer();
            break;
        }
    }
    returnSectionIndex() {
        let indexToReturn = -1;
        for (let i = 0; i < this.sections.length; i++) {
            let bottomOfSection = this.sections[i].offsetTop + this.sections[i].offsetHeight;
            if (this.scrollPosition > this.sections[i].offsetTop && this.scrollPosition < bottomOfSection) {
                indexToReturn = i;
                break;
            }
        }
        return indexToReturn;
    }
    returnCircleArray() {
        let circleArray = [];
        for (let i = 0; i < this.circleElements.length; i++) {
            circleArray.push(new CircleElement(this.circleElements[i], undefined, undefined));
        }
        return circleArray;
    }
    setCircleArray() {
        this.circleArray = this.returnCircleArray();
    }
    mathMapping (n, start1, stop1, start2, stop2) {
        const newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
        return newval;
    }
    reScale() {
        let debouncer;
        clearTimeout(debouncer)
        debouncer = setTimeout(() => {
            this.containerElement.style.left = window.innerWidth / 2 + 'px';
            this.containerElement.style.top = window.innerHeight / 2 + 'px';
        }, 100)
    }
    events() {
        window.addEventListener('scroll', event => {
            let settimeout;
            clearTimeout(settimeout);
            settimeout = setTimeout(() => {
                this.onScroll()}, 50);
        })
        window.addEventListener('resize', event => this.reScale());
    }
}

//this isn't actually doing anything is it?
/* this.rafObj = {
    elements: [],
    init: function() {
        this.animate();
    },
    add: function(obj) {
        this.elements.push(obj);
    },
    remove: function(obj) {
        var index = this.elements.indexOf(obj);
        this.elements.splice(index, 1);
    },
    animate: function() {
        requestAnimationFrame(this.rafObj.animate);
        this.rafObj.render();
    },
    render: function() {
        var time = new Date().getTime() * 0.005;
        for (var index = 0; index < this.elements.length; index++) {
            var element = this.elements[index];
            element.render(time);
        }
    }
} */