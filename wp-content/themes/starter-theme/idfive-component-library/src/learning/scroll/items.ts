export default class CircleElement { 
    protected yPos: number;
    protected xPos: number;
    protected element: HTMLElement;
    constructor(element, xPos, yPos) {
        this.element = element;
        this.xPos = xPos;
        this.yPos = yPos;
    }
    public positioning() {
        this.element.style.left = this.xPos + 'px';
        this.element.style.top = this.yPos + 'px';
    }
}