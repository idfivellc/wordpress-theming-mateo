import { stringify } from "querystring";

export default class ProgramExplorer {
    private finalArray = [];
    constructor(element) {
        this.getTableElements(); 
        //this.printJSON();   
    }
    getTableElements() {
        let arrayToReturn = [];
        let tableData = document.querySelectorAll('tr');
        for (let i = 0; i < tableData.length; i++) {
            const tableElement = tableData[i];
            let objTemp = {
                'Program Name': String,
                'Program Type': String,
                'Subject': String,
            }
            let index = 0;
            if (i > 0) {
                for (const key in objTemp) {
                    let value = tableElement.cells[index].innerHTML;
                    objTemp[key] = value;
                    index += 1;
                }
                arrayToReturn.push(objTemp);
            }
        }
        return arrayToReturn;
    }
    printJSON() {
        const outputID = document.getElementById('json') as HTMLElement;
        outputID.innerHTML = JSON.stringify(this.getTableElements(), undefined, 2);
    }
}