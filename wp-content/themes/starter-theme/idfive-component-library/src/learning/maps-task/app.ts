import Vue from 'vue'
import Map from './map';
import Data from './data';
export default class VueApp {
    private vueApp;
    constructor(vueElement) {
        this.vueApp = new Vue({
            el: vueElement,
            data: {
                map: undefined,
                neighborhoodList: undefined,
                data: undefined,
            },
            methods: {
                retrieveData() {
                    this.data = new Data();
                    this.neighborhoodList = this.data.neighborhoodsList;
                    //console.log(this.neighborhoodList)
                },
                setNeighborhoodsList() {
                    
                }
            },
            mounted() {
                this.retrieveData();
                setTimeout(() => {
                    this.map = new Map(this.data.data);
                }, 1000)
            }
        })
    }
}