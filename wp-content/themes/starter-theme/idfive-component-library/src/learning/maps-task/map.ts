import * as GoogleMapsLoader from 'google-maps';

export default class Map {
    protected data: any;
    protected map: any;
    protected mapElement: HTMLElement;
    protected coordinatesArray: Array<any> = [];
    protected neighborhoodCoordinates: Array<any> = [];
    private areaElement: any;
    private coordsReady: boolean;
    private areaCoords: any;
    constructor(data) {
        this.data = data;
        this.mapElement = document.querySelector('#map');
        this.mapInit(this.data);
    }
    mapInit(data) {
/*         GoogleMapsLoader.KEY = 'AIzaSyA1aS-f-NnpxT31Xm56aYK5w5-hsF48zHw';
        GoogleMapsLoader.VERSION = 'weekly';
        GoogleMapsLoader.LIBRARIES = ['map']; */
        GoogleMapsLoader.load((google) => {
            this.map = new google.maps.Map(this.mapElement, {
                center: {lat: 39.29038, lng: -76.61219},
                zoom: 8
            });
            setTimeout(() => {
                this.getAllCoordinates();
                this.getNeighborhoodCoordinates();
            }, 1000)
            //this.returnFullListOfLatLng();
            //this.drawPolygons(data);
        })
    }
    getAllCoordinates() {
        for (let i = 0; i < this.data.length; i++) {
            this.coordinatesArray.push(this.data[i].geometry.coordinates[0]);
        }
    }
    getNeighborhoodCoordinates() {
        for (let i = 0; i < this.coordinatesArray.length; i++) {
            for (let j = 0; j < this.coordinatesArray[i].length; j++) {
                    this.coordinatesArray[i][j] = {lat: this.coordinatesArray[i][j][0], lng: this.coordinatesArray[i][j][1]}
                    this.coordsReady = true;
            }
        }
        for (let j = 0; j < 10; j++) {
            this.drawPolygons(this.coordinatesArray[j])
        }
/*         for (let i = 0; i < this.coordinatesArray.length; i++) {
            let coordinatesObjPerNeighborhood = {
                array: this.coordinatesArray[i]
            }
            allNeighborhoodsArray.push(this.coordinatesArray[i]);
            for (let j = 0; j < coordinatesObjPerNeighborhood['array'].length; j++) {
                eachNeighborhoodCoordsArray.push(coordinatesObjPerNeighborhood['array'][j]);
                let neighborhoodCoordinatesObj = {
                    lat: coordinatesObjPerNeighborhood['array'][j][0],
                    lng: coordinatesObjPerNeighborhood['array'][j][1],
                }
                test.push(neighborhoodCoordinatesObj);
            }
            //neighborhoodCoordinatesKey.push(coordinatesObjPerNeighborhood);
            // for (let j = 0; j < neighborhoodCoordinatesKey.length; j++) {
            //     this.neighborhoodCoordinates.push(neighborhoodCoordinatesKey[i].array[j])
            // }
            /* this.areaElement.setMap(this.map); */
        }
        //console.log('copn', test);
        //console.log(this.neighborhoodCoordinates);  
    drawPolygons(coords) {
        console.log(coords);
        this.areaCoords = [
            {lat: 3.4540, lng: -76.5320},
            {lat: 3.4519, lng: -76.5325},
            {lat: 3.4506, lng: -76.5363},
            {lat: 3.4532, lng: -76.5373},
        ]
        this.areaElement = new google.maps.Polygon({
            paths: this.areaCoords,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        });
        this.areaElement.setMap(this.map);
    }
}