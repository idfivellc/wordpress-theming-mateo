import Axios from 'axios';
export default class Data {
    protected data: any;
    protected neighborhoodsList: Array<any> = [];
    constructor() {
        this.getData();
    }
    getData() {
        Axios.get('https://mateoodlc.github.io/baltimore.geojson')
        .then(response => {
            this.data = response.data.features;
            //console.log(response);
            //console.log('data', this.data);
            this.setNeighborhoodList();
        })
        .catch(error => {
            console.log(error);
        })
    }
    setNeighborhoodList() {
        for (let i = 0; i < this.data.length; i++) {
            this.neighborhoodsList.push(this.data[i].properties.LABEL);
        }
        console.log(this.neighborhoodsList);
    }
}