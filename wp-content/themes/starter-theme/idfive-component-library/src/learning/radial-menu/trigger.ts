export default class Trigger {
    protected posX: number;
    protected posY: number;
    constructor(element, posX, posY) {
      this.posX = posX;
      this.posY = posY;
      this.createMenu(element);
    }
    createMenu(element) {
      element.style.left = this.posX - (element.offsetWidth / 2) + 'px';
      element.style.top = this.posY - (element.offsetHeight / 2) + 'px';
    }
  }