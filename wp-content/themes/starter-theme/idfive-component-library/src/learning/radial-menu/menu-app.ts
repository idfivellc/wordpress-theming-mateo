import Trigger from './trigger'
import Item from './items'
import World from './background'
export default class MenuApp {
    protected menuTrigger: HTMLElement;
    protected items: Array<any>;
    protected menuContainer: HTMLElement;
    protected elementNumber: number;
    protected factor: number = 0;
    protected isDisplayed: boolean;
    protected background: any;
    protected scroll: any;
    protected center = {
      x: window.innerWidth / 2,
      y: window.innerHeight / 2,
    }
    constructor(container, trigger) {
      console.log('container');
      this.background = new BackgroundApp();
      this.elementNumber = 5;
      this.menuTrigger = trigger;
      this.menuContainer = container;
      this.menuTrigger.addEventListener('click', event => this.displayItems())
      this.items = [];
      this.createItems();
      this.createMenu();
    }
    createMenu() {
      const trigger = new Trigger(this.menuTrigger, this.center.x, this.center.y);
    }
    createItems() {
      for (let i = 0; i < this.elementNumber; i++) {
        this.items.push(new Item(this.menuContainer, this.center.x, this.center.y, 100, i, this.elementNumber)); 
      }
    }
    displayItems() {
      this.isDisplayed = !this.isDisplayed;
      this.factor += 1;
      for (let i = 0; i < this.items.length; i++) {
        this.items[i].display(this.isDisplayed); 
      }
      if (this.isDisplayed) {
        this.menuContainer.classList.add('menu-displayed');
        setTimeout(() => {
          this.menuContainer.style.transform = `rotate(${0}deg)`;
        }, 600)
      } else {
        this.menuContainer.classList.remove('menu-displayed');
        setTimeout(() => {
          this.menuContainer.style.transform = `rotate(${-140}deg)`;
        }, 0)
      }
    }
  }

  class BackgroundApp {
    protected world: any;
    protected canvas: any;
    protected context: CanvasRenderingContext2D;
    constructor() {
      this.canvas = document.getElementById('myBackgroundCanvas');
      this.context = this.canvas.getContext('2d');
      this.canvas.width = window.innerWidth;
      this.canvas.height = window.innerHeight;
      this.world = new World(this.context);
    }
  }