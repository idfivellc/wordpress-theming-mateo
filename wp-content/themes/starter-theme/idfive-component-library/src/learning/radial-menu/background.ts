interface CircleAtt {
    posX: number,
    posY: number,
    velX: number,
    velY: number,
    distance: number,
    color: string,
    size: number,
    radius: number,
    context: CanvasRenderingContext2D,
  }

export default class World {
    protected circles: Array<any> = [];
    protected circleObj: any;
    protected radius: number;
    protected distance: number;
    protected length: number;
    constructor(context) {
        console.log(context);
      this.length = ~~Number((window.innerWidth * window.innerHeight) / 5000);
      //window.addEventListener('mousemove', event => this.getDistance(event));
      this.createCircles(context);
      window.requestAnimationFrame(() => this.animate(context));
    }
    createCircles(context) {
      for (let i = 0; i < this.length; i++) {
        this.radius = this.randomRadius();
        this.circleObj = {
          radius: this.radius,
          posX: this.randomPosX(),
          posY: this.randomPosY(),
          velX: (Math.random() - 0.5),
          velY: (Math.random() - 0.5),
          distance: this.distance,
          color: "#000",
          size: 20,
          context: context,
        }
        this.circles.push(new Circle(this.circleObj));
      }
    }
    delegateMousePos(event) {
      for (let i = 0; i < this.length; i++) {
        this.circles[i].updateMousePos(event.clientX, event.clientY);
      }
    }
    animate(context) {
      this.circleObj.context.clearRect(0, 0, window.innerWidth, window.innerHeight);
      for (let i = 0; i < this.length; i++) {
        this.circles[i].update(this.distance);
      }
      window.requestAnimationFrame(() => {
        this.animate(context);
      })
    }
    randomPosX() {
      return Math.random() * (window.innerWidth - this.radius * 2) + this.radius;
    }
    randomPosY() {
      return Math.random() * (window.innerHeight - this.radius * 2) + this.radius;
    }
    randomRadius() {
      return Math.floor(Math.random() * 15) + 1;
    }
  }

class Circle {
    protected circleObj: any;
    protected mousePos: any = {
        x: 0,
        y: 0,
    }
    constructor(circleObj: CircleAtt) {
        this.circleObj = circleObj;
    }
    draw() {
        this.circleObj.context.beginPath();
        this.circleObj.context.arc(this.circleObj.posX, this.circleObj.posY, this.circleObj.radius, 0, 2 * Math.PI);
        this.circleObj.context.fillStyle = this.circleObj.color;
        this.circleObj.context.fill();
    }
    validateDistance(mousePos) {
        let x = this.circleObj.posX - mousePos.x;
        let y = this.circleObj.posY - mousePos.y;
        return Math.sqrt(x*x + y*y);
    }
    update(distance) {
        if (this.circleObj.posX + this.circleObj.radius > window.innerWidth || this.circleObj.posX - this.circleObj.radius < 0) {
        this.circleObj.velX = -this.circleObj.velX;
        }
        if (this.circleObj.posY + this.circleObj.radius > window.innerHeight || this.circleObj.posY - this.circleObj.radius < 0) {
        this.circleObj.velY = -this.circleObj.velY;
        }
        this.circleObj.posX += this.circleObj.velX
        this.circleObj.posY += this.circleObj.velY;
        if (this.circleObj.distance < 40 && this.circleObj.radius < 40) {
        this.circleObj.radius += 1;
        } else if (this.circleObj.distance > 40 && this.circleObj.radius > 2) {
        this.circleObj.radius -= 1;
        }
        this.draw();
    }
    updateMousePos(x, y) {
        this.mousePos.x = x;
        this.mousePos.y = y; 
    }
}
