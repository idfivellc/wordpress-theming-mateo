export default class Item {
    protected element: HTMLElement;
    protected posX: number;
    protected posY: number;
    protected xInit: number;
    protected yInit: number;
    protected centerDistance: number;
    protected maxAngle: number;
    protected angleDistance: number;
    protected color: String;
    protected size: number;
    protected isDisplayed: boolean;
    protected colors: Array<any> = [];
    constructor(menuContainer, posX, posY, menuRadius, i, maxElements) {
      this.setInitialState(menuContainer, posX, posY, menuRadius, i, maxElements)
      this.colors = [
        '#F44336',
        '#2196F3',
        '#9C27B0',
        '#8BC34A',
        '#FF5722',
        '#FFEB3B',
      ]
      this.setColors(i);
      this.element.addEventListener('click', event => this.changeState());
      this.element
    }
    createHTMLElement(menuContainer) {
      this.element = document.createElement('div')
      menuContainer.appendChild(this.element);
      this.element.classList.add('menu-element');
      this.initialState();
    }
    display(isDisplayed) {
      if (isDisplayed) {
        this.element.style.left = this.posX - (this.element.offsetWidth / 2) + 'px';
        this.element.style.top = this.posY - (this.element.offsetHeight / 2) + 'px'; 
      } else {
        this.element.style.left = this.xInit + 'px';
        this.element.style.top = this.yInit + 'px';
      }
    }
    setInitialState(menuContainer, posX, posY, menuRadius, i, maxElements) {
      this.maxAngle = Math.PI * 2;
      this.angleDistance = this.maxAngle / maxElements;
      this.size = this.size < 60 ? 500 / maxElements : 60;
      this.xInit = posX - this.size / 2;
      this.yInit = posY - this.size / 2;
      this.centerDistance = menuRadius;
      this.posY = posY - (this.centerDistance * Math.sin((i + 120) * this.angleDistance));
      this.posX = posX - (this.centerDistance * Math.cos((i + 120) * this.angleDistance));
      this.createHTMLElement(menuContainer);
    }
    setColors(i) {
      this.element.style.background = this.colors[i];
    }
    initialState() {
      this.element.style.width = this.size + 'px';
      this.element.style.height = this.size + 'px';
      this.element.style.top = this.yInit + 'px';
      this.element.style.left = this.xInit + 'px';
    }
    changeState() {
      
    }
  }