import { request } from "https";

export default class MagicCursor {
    protected posX: number = 0; 
    protected posY: number = 0;
    protected mouseX: number = 0;
    protected mouseY: number = 0;
    protected operatedX: number = 0;
    protected operatedY: number = 0;
    protected minSize: number;
    protected maxSize: number;
    protected size: number;
    protected context;
    protected dX: number = 0;
    protected dY: number = 0;
    protected index: number;
    constructor(mousePos, context) {
        this.context = context;
        this.mouseX = mousePos.x;
        this.mouseY = mousePos.y;
        this.minSize = 15;
        this.maxSize = 15;
        console.log(this.context);
    }
    createBubbles(mousePos, index, minSize, maxSize) {
        this.size = index * 5 / 10 - (index / 5);
        this.size = this.size >= maxSize ? maxSize : this.size;
        this.size = this.size <= minSize ? minSize : this.size;
        this.dX = mousePos.x - this.posX;
        this.dY = mousePos.y - this.posY;
        this.posX += (this.dX / 100);
        this.posY += (this.dY / 100);
        this.operatedX = this.posX - 10 - this.dX * ((this.size * -5) / 100);
        this.operatedY = this.posY - 10 - this.dY * ((this.size * - 5) / 100);
        this.context.beginPath();
        this.context.arc(this.operatedX, this.operatedY, this.size, 0, 2 * Math.PI);
        this.context.fill();
        this.context.fillStyle = "#0000ff";
    }
}