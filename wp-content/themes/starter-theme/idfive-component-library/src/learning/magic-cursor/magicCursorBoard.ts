import MagicCursor from './magicCursor';
import Bubbles from './bubbles';
export default class MagicCursorBoard {
    protected canvas: any;
    protected context: CanvasRenderingContext2D;
    protected mousePos: any;
    protected cursorBubble: any;
    protected bubble: Object;
    protected ease: number = 200;
    protected debouncer: any;
    constructor(canvasElement) {
        this.canvas = canvasElement;
        this.context = this.canvas.getContext('2d')
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        console.log(this.context);
        this.mousePos = {
            x: 0,
            y: 0,
        }
        this.cursorBubble = new MagicCursor(this.mousePos, this.context);
        this.addEvents();
        this.draw();
    }
    explode() {
        for (let i = 0; i < 10; i++) {
            this.bubble = new Bubbles(this.mousePos.x, this.mousePos.y, this.context);    
        }
    }
    getMousePos(e) {
        this.mousePos = {
            x: e.clientX,
            y: e.clientY,
        }
    }
    draw() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        for (let i = 0; i <= 30; i++) {
            //this.cursorBubble.createBubbles(this.mousePos, i);
        }
        requestAnimationFrame(() => {this.draw()})
    }
    addEvents() {
        this.canvas.addEventListener('mousemove', event => {
            this.getMousePos(event)
            this.explode();
        })
  /*    this.canvas.addEventListener('click', event => {
        this.explode();  
      }) */
    }
}