import * as SimplexNoise from 'simplex-noise';
export default class Bubbles {
    protected vector: Object;
    protected posX;
    protected posY;
    protected size;
    protected blue;
    protected opacity;
    protected isExploding;
    protected context;
    constructor(posX, posY, context) {
        this.context = context;
        this.posX = posX;
        this.posY = posY;
        this.opacity = 90;
        let randomSize = this.randomSize();
        let randomX = this.randomNoise() * this.randomDirection();
        let randomY = this.randomNoise() * this.randomDirection();
        let randomSpeed = this.randomSpeed();
        let randomBlue = this.randomBlue();
        this.draw(this.posX, this.posY, randomX, randomY, randomSpeed, randomSize, randomBlue);
        this.isExploding = true;
        setTimeout(() => {
        this.isExploding = false;
        }, 2000);
    }
    draw(posX, posY, randomX, randomY, randomSpeed, randomSize, randomColor) {
        this.opacity --;
        this.posX += randomX / randomSpeed;
        this.posY += randomY / randomSpeed;
        this.context.beginPath();
        this.context.arc(this.posX, this.posY, randomSize, 0, 2 * Math.PI);
        this.context.fillStyle = 'rgba( ' + 50 + ',' + 50 + ',' + randomColor + ',' + this.opacity / 100 + ')';
        this.context.fill();
        requestAnimationFrame(() => {
        if(this.isExploding) {
            this.draw(posX, posY, randomX, randomY, randomSpeed, randomSize, randomColor);
        }
        }); 
    }
    randomNoise() {
        const simplex = new SimplexNoise();
        return simplex.noise2D(5, 1000);
    }
    randomDirection() {
        return Math.floor(Math.random() * (100 - (-100)) ) + (-100);
    }
    randomSpeed() {
        return Math.floor(Math.random() * (15 - 10) + 10);
    }
    randomBlue() {
        return Math.floor(Math.random() * (255 - 100) + 100)
    }
    randomSize() {
        return Math.floor(Math.random() * (7 - 1) + 1)
    }
}