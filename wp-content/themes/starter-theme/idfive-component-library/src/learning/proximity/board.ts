import Dot from './dots';
export default class Proximity {
  protected mousePos: Object;
  protected canvas: any;
  protected ctx: CanvasRenderingContext2D;
  protected canvasWidth: number;
  protected canvasHeight: number;
  protected dots: Array<any> = [];
  protected isDrawing: boolean;
  constructor(myCanvasProximity) {
    this.canvas = myCanvasProximity;
    this.ctx = this.canvas.getContext('2d');
    this.canvas.width = this.canvasWidth = window.innerWidth;
    this.canvas.height = this.canvasHeight = window.innerHeight;
    this.mousePos = {
      x: 0,
      y: 0,
    }
    this.buildBoard(this.canvasWidth, this.canvasHeight);
    window.addEventListener('mousemove', event => {
      this.distance(event);
    });
    window.addEventListener('touchmove', event => {
      this.distance(event);
    });
    this.animate();
    this.setEvents();
  }
  buildBoard(canvasWidth, canvasHeight) {
    let size = 20;
    let radius = 1;
    const columns = Math.ceil(canvasWidth / size) + 1;
    const rows = Math.ceil(canvasHeight / size) + 1;
    const amount = Math.ceil(columns * rows);
    for (let i = 0; i < amount; i++) {
      const column = i % columns;
      const row = ~~(i / columns);
      this.dots.push(new Dot(radius, 30 + size * column, 30 + size * row));
    }
  }
  distance(event) {
    let minDistance: number;
    let maxDistance: number = window.innerWidth / 10;
    let minSize: number = 0;
    let maxSize: number = 50;
    for (let dot of this.dots) {
      minDistance = dot._radius;
      let subDistance = Math.sqrt(Math.pow(dot.position.x - event.clientX, 2) + Math.pow(dot.position.y - event.clientY, 2))
      let distance = this.map(subDistance, minDistance, maxDistance, maxSize, minSize);
      if(distance < 0) distance = 0;
      dot.addRadius(distance);
    }
  }
  animate() {
    if(!this.isDrawing) {
      this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight); 
    }
    this.ctx.beginPath();
    this.ctx.fillStyle = '#000';
    for (let dot of this.dots) {
      dot.draw(this.ctx, 0.035);
    }
    this.ctx.fill();
    requestAnimationFrame(() => this.animate());
  }
  map (n, start1, stop1, start2, stop2) {
    const newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
    return newval;
  }
  allowDrawing() {
    this.isDrawing = !this.isDrawing;
  }
  setEvents() {
    window.addEventListener('mousedown', event => {
      this.allowDrawing();
    })
    window.addEventListener('mouseup', event => {
      this.allowDrawing();
    })
  }
}