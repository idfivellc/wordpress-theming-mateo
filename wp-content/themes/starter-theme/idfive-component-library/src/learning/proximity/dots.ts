export default class Dot {
  protected _radius: number;
  protected radius: number;
  protected growthValue: number;
  protected position: any;
  constructor(radius, x, y) {
    this._radius = radius;
    this.radius = radius;
    this.growthValue = 0;
    this.position = {
      x: x,
      y: y,
    }
  }
  draw(context, ease) {
    this.radius += ((this._radius + this.growthValue) - this.radius) * ease;
    context.moveTo(this.position.x, this.position.y);
    context.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
  }
  addRadius(value) {
    this.growthValue = value;
  }
}

///////// OLD METHOD
//        this.growthValue = (this.growthValue - currentDistance) * this.ease;
/*      if(this.growthValue < currentDistance/10 * this.ease / 5) {
        this.growthValue ++;
      } else if (distance > 60) {
        if (this.growthValue > 1) {
          setTimeout(() => {
            this.growthValue -= this.growthValue > 1 ? 1 : 0; 
          }, 100)
        }      
      }*/