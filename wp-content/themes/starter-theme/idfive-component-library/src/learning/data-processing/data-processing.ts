import Vue from 'vue';
import Axios from 'axios';
export default class DataVue {
    private vueApp: any;
    constructor(vueElement) {
        this.vueApp = new Vue({
            el: vueElement,
            data: {
                rawMeasuresData: [],
                selectedMeasures: [],
                measuresFullObj: undefined,
            },
            methods: {
                getData() {
                    Axios.get('/wordpress-theming/wp-content/themes/starter-theme/assets/measures2018.json')
                    .then(response => {
                        this.rawMeasuresData = response.data;
                    })
                },
                prepareAverageArray() {
                    for (let i = 0; i < this.selectedMeasures.length; i++) {
                        for (const key in this.measuresFullObj) {
                            this.measuresFullObj[key].average = `${this.selectedMeasures.length} fips to calculate`;
                            let valueToAdd = this.selectedMeasures[i][key] 
                            if (!this.measuresFullObj[key].values.includes(Number(valueToAdd))) {
                                if (valueToAdd !== "") {
                                    this.measuresFullObj[key].values.push(Number(valueToAdd));
                                }
                            }
                        }
                    }
                },
                calculateAverage() {
                    for (const key in this.measuresFullObj) {
                        const element = this.measuresFullObj[key];
                        if (element.values.length > 0) {
                            const sumTemp = element.values.reduce((total, sum) => total + sum)
                            this.measuresFullObj[key].average = sumTemp / element.values.length;
                        }
                    }
                },
                selectAll() {
                    const checkboxElems = this.$refs['key'];
                    if (checkboxElems.length > 0) {
                        for (let i = 0; i < checkboxElems.length; i++) {
                            const element = checkboxElems[i];
                            if (element.checked == true) {
                                element.checked = false;
                                this.selectedMeasures = [];
                            } else if (element.checked == false){
                                element.checked = true;
                                this.selectedMeasures.push(element._value)
                            }
                        }
                    }
                    this.prepareAverageArray();
                }
            },
            mounted() {
                this.getData();
            },
            computed: {
                selectedMeasuresComputed() {
                    return this.selectedMeasures;
                },
                baseObject() {
                    let measureFullObj = Object.assign({}, this.rawMeasuresData[0]); 
                    for (const key in measureFullObj) {
                        measureFullObj[key] = {
                            'values': [],
                            'average': undefined,
                        };
                    }
                    this.measuresFullObj = Object.assign({}, measureFullObj); 
                    return this.measuresFullObj;
                }
            },
        })
    }
}