import Vue from 'vue';
export default class ColorMap {
    protected vueApp;
    constructor(vueElement) {
        this.vueApp = new Vue({
            el: vueElement,
            data: {
                colorsArray: [],
                color1: [],
                color2: [],
                maxColor: undefined,
                minColor: undefined,
                maxValue: 10,
                minValue: 255,
                valuesForSelect: {
                    1: [0.234, 2.523, 9.342],
                    2: [0.4532, 1.683, 3.588]
                },
            },
            mounted() {
                console.log('holi');
            },
            methods: {
                setColorsArray() {
                    this.color1 = [30, 120, 200];
                    this.color2 = [200, 30, 120];
                },
                paintWithColors() {
                    this.minColor.style.backgroundColor = `rgb(${this.color1[0], this.map()})`
                },
                map (n, start1, stop1, start2, stop2) {
                    const newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
                    return newval;
                }
            },
            computed: {
                colorArrayComputed() {
                    return this.colorsArray;
                }
            }
        })
    }
}