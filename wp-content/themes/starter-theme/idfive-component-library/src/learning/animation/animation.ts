import { gsap, Power1, Power3, Power4 } from "gsap";
import Vue from 'vue';
export default class AnimationApp {
    protected vueApp;
    protected circleElement: HTMLElement = document.querySelector('.pointer-circle');
    protected pointer: HTMLElement = document.querySelector('.pointer');
    private screenWidth: number = window.innerWidth;
    private screenHeight: number = window.innerHeight;
    constructor(vueElement) {
        this.vueApp = new Vue({
            el: vueElement,
            data: {
                timeline: gsap.timeline({repeat: 0})
            },
            methods: {
                createTimeLine() {
                    this.timeline.to(this.$refs.backgroundWrapperRef, {webkitClipPath: 'inset('+ 0 + '%' + 99 + '%' + 60 + '%' + 0 + '%'+')', duration: 1, ease: Power3.easeInOut})
                    this.timeline.to(this.$refs.backgroundWrapperRef, {webkitClipPath: 'inset('+ 0 + '%' + 60 + '%' + 60 + '%' + 0 + '%'+')', duration: 0.7, ease: Power3.easeInOut});
                },
                setMouseCircle(event) {
                    let debouncer;
                    clearTimeout(debouncer)
                },
            },
            mounted() {
                this.createTimeLine();
            }
        })
    }
    addEvents() {
        window.addEventListener('mousemove', event => {
            this.followPointer(event);
        })
    }
    followPointer(e) {
        const rawX = e.offsetX;
        const rawY = e.offsetY;
        const halfW = this.screenWidth / 2;
        const halfH = this.screenHeight / 2;
        gsap.to(this.circleElement, 0.6, {
            x: rawX - halfW,
            y: rawY - halfH, ease: Power1.easeInOut
        });
        gsap.to(this.pointer, 0.05, {
            x: rawX - halfW,
            y: (rawY - halfH)
        });
    }
}