export default class ScrollAura {
    private element: HTMLElement;
    private sectionsArray: NodeListOf<HTMLElement>;
    private scrollPosition: number;
    private minScrollPositionValue: number;
    private maxScrollPositionValue: number;
    private minElementValue: number;
    private maxElementValue: number;
    private scaleValue: number = 0;
    constructor(element, sections) {
        this.element = element;
        this.sectionsArray = sections;
        this.setScrollPosition();
        this.addEvents();
        this.setValues();
        this.reScaleDevice();
    }
    setScrollPosition() {
        this.scrollPosition = window.pageYOffset;
    }
    setValues() {
        this.minElementValue = 1;
        this.maxElementValue = 2;
        this.minScrollPositionValue = this.getPureElementDistanceFromTop(this.sectionsArray[1]);
        this.maxScrollPositionValue = this.minScrollPositionValue + this.sectionsArray[1].offsetHeight / 2;
    }
    stickDevice() {
        if (this.scrollPosition >= this.getPureElementDistanceFromTop(this.sectionsArray[1])) {
            if (!this.element.classList.contains('aura-element--fixed')) {
                this.element.classList.add('aura-element--fixed');
            }
        } else {
            if (this.element.classList.contains('aura-element--fixed')) {
                this.element.classList.remove('aura-element--fixed');
            }
        }
        if (this.scrollPosition + window.innerHeight > this.getPureElementDistanceFromTop(this.sectionsArray[1]) + this.sectionsArray[1].offsetHeight) {
            if (this.element.classList.contains('aura-element--fixed')) {
                this.element.classList.remove('aura-element--fixed');
                //this.element.classList.add('aura-element--absolute--bottom');
            }
        }
    }
    reScaleDevice() {
        if (this.scrollPosition + window.innerHeight / 4 >= this.getPureElementDistanceFromTop(this.sectionsArray[1])) {
                if (this.scrollPosition < this.getPureElementDistanceFromTop(this.sectionsArray[1]) + this.sectionsArray[1].offsetHeight / 2) {
                    this.scaleValue = this.map(this.scrollPosition, this.minScrollPositionValue, this.maxScrollPositionValue, this.minElementValue, this.maxElementValue);
                    this.element.style.transform = `translateX(-50%) scale(${this.scaleValue})`;
                }
        }
        //window.requestAnimationFrame(() => this.reScaleDevice());
    }
    addEvents() {
        window.addEventListener('scroll', event => {
            this.setScrollPosition();
            this.stickDevice();
            this.reScaleDevice();
        })
    }
    map (n, start1, stop1, start2, stop2) {
        const newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
        return newval;
    }
    getPureElementDistanceFromTop(element) {
        let distanceToReturn = 0;
        if (element.offsetParent) {
            do {
                distanceToReturn += element.offsetTop;
                element = element.offsetParent;
            } while (element);
        }
        if (distanceToReturn >= 0) {
            return distanceToReturn;
        } else {
            return 0;
        }
    }
}