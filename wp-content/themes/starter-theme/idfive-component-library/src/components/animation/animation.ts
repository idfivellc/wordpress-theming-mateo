import { gsap } from "gsap";
import Vue from 'vue';
export default class AnimationApp {
    protected vueApp;
    constructor(vueElement) {
        this.vueApp = new Vue({
            el: vueElement,
            data: {
                timeline: gsap.timeline({repeat: 0})
            },
            methods: {

            },
            mounted() {
                
            }
        })
    }
}