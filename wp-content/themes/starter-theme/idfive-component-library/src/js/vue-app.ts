import Vue from "vue";
import programComponent from './program-component.vue';

class ProgramExplorerJSON {
  public tableElements = document.querySelectorAll('tr') as NodeListOf<HTMLElement>;
  private tableHeaders = document.querySelectorAll('th') as NodeListOf<HTMLElement>;
  constructor() {

  }
  returnInnerHTMLObj(arr) {
    let tempArray = [];
    let tableHeaders = this.tableHeaders;
    for (let i = 0; i < arr.length; i++) {
      const tableElement = arr[i];
      let tempObj = {
        [tableHeaders[0].innerHTML]: [],
        [tableHeaders[1].innerHTML]: [],
        [tableHeaders[2].innerHTML]: []
      }
      let index = 0;
      for (const key in tempObj) {
        let value = tableElement.cells[index].innerHTML;
        tempObj[key] = value;
        index += 1;
      }
      tempArray.push(tempObj);
    }
    let objToReturn = {};
    objToReturn['headings'] = tempArray[0];
    tempArray.splice(0, 1);
    objToReturn['values'] = tempArray;
    return objToReturn; 
  }
}

const programExplorer = new ProgramExplorerJSON();
const tableExportedJSON = programExplorer.returnInnerHTMLObj(programExplorer.tableElements);
console.log(tableExportedJSON);


const programExplorerVueApp = new Vue({
  el: '#vue-app',
  data: {
    dataJSON: tableExportedJSON['values'],
    headings: tableExportedJSON['headings'],
  },
  components: {
    'program-component': programComponent
  },
  template: `
        <program-component :headings="headings" :data="dataJSON"/>
      `,
  mounted() {
  },
});

export { programExplorerVueApp };
