import Vue from 'vue';
import Axios from 'axios';
export default class DataVue {
    private vueApp: any;
    constructor(vueElement) {
        this.vueApp = new Vue({
            el: vueElement,
            data: {
                rawMeasuresData: [],
                selectedMeasures: [],
                measureFullObj: [],
                averageArray: [],
            },
            methods: {
                getData() {
                    Axios.get('/wordpress-theming/wp-content/themes/starter-theme/assets/measures2018.json')
                    .then(response => {
                        this.rawMeasuresData = response.data;
                    })
                    .then (() => {
                        this.createMeasuresObject();
                    })
                },
                createMeasuresObject() {
                    this.measureFullObj = Object.assign({}, this.rawMeasuresData[0]); 
                    for (const key in this.measureFullObj) {
                        this.measureFullObj[key] = {
                            'values': [],
                            'average': undefined,
                        };
                    }
                },
                prepareAverageArray() {
                    this.createMeasuresObject();
                    for (let i = 0; i < this.selectedMeasures.length; i++) {
                        for (const key in this.measureFullObj) {
                            let valueToAdd = this.selectedMeasures[i][key] 
                            if (!this.measureFullObj[key].values.includes(Number(valueToAdd))) {
                                if (valueToAdd !== "") {
                                    this.measureFullObj[key].values.push(Number(valueToAdd));
                                }
                            }
                        }
                    }
                },
                calculateAverage() {
                    for (const key in this.measureFullObj) {
                        const element = this.measureFullObj[key];
                        if (element.values.length > 0) {
                            const sumTemp = element.values.reduce((total, sum) => total + sum)
                            this.measureFullObj[key].average = sumTemp / element.values.length;
                        }
                    }
                },
                selectAll() {
                    const checkboxElems = this.$refs['key'];
                    if (checkboxElems.length > 0) {
                        for (let i = 0; i < checkboxElems.length; i++) {
                            const element = checkboxElems[i];
                            if (element.checked == true) {
                                element.checked = false;
                                this.selectedMeasures = [];
                            } else if (element.checked == false){
                                element.checked = true;
                                this.selectedMeasures.push(element._value)
                            }
                        }
                    }
                    this.prepareAverageArray();
                }
            },
            mounted() {
                this.getData();
            },
            computed: {
                selectedMeasuresComputed() {
                    return this.selectedMeasures;
                }
            },
        })
    }
}