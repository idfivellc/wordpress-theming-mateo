import Vue from 'vue';
import Axios from 'axios';
export default class DataVue {
    private vueApp: any;
    constructor(vueElement) {
        this.vueApp = new Vue({
            el: vueElement,
            data: {
                rawMeasuresData: [],
            },
            methods: {
                getData() {
                    Axios.get('/wordpress-theming/wp-content/themes/starter-theme/assets/measures2018.json')
                    .then(response => {
                        this.rawMeasuresData = response.data;
                        console.log(this.rawMeasuresData);
                    })
                },
            },
            mounted() {
                this.getData();
            },
            computed: {
                averageArrayComputed() {
                    let arrayToReturn = [];
                    if (this.rawMeasuresData.length) {
                        for (const measure in this.rawMeasuresData[0]) {
                            let integerElement1 = 0;
                            let integerElement2 = 0;
                            if (this.rawMeasuresData[0][measure]) {
                                integerElement1 = Number(this.rawMeasuresData[0][measure]); 
                            }
                            if (this.rawMeasuresData[1][measure]) {
                                integerElement2 = Number(this.rawMeasuresData[1][measure]); 
                            }
                            let averageTemp = (integerElement1 + integerElement2) / (integerElement1 && integerElement2 !== 0 ? 2 : 1);
                            let averageObj = {
                                'key' : measure,
                                'fip1': integerElement1,
                                'fip2': integerElement2,
                                'average': averageTemp
                            }
                            arrayToReturn.push(averageObj);
                        }
                    }
                    return arrayToReturn;
                }
            }
        })
    }
}