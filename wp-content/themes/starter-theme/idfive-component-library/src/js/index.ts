/**
 * Modules
 */
import {gsap, Power1, Power2, Power3, Power4} from 'gsap';
import { silcCoreInit } from "silc-core";
import { silcAccordionInit } from "silc-accordion";
import { silcNavInit } from "silc-nav";
import { silcOffcanvasInit } from "silc-offcanvas";
import focusWithin from "focus-within";

import {SilcCarouselInit} from "../components/carousel/carousel";
import Tablesaw from "../components/table/table";
import Modal from "../components/modal/modal";
import AnimationApp from "../learning/animation/animation";

/**
 * Init
 */
focusWithin(document);
silcCoreInit();
silcAccordionInit();
silcNavInit();
silcOffcanvasInit();
SilcCarouselInit();
Tablesaw.init();
Modal.init();

import Proximity from '../learning/proximity/board';
import MagicCursorBoard from "../learning/magic-cursor/magicCursorBoard";
import MenuApp from "../learning/radial-menu/menu-app";
import ScrollApp from '../learning/scroll/scrollApp';
import RobertosScroll from '../learning/scroll/robertosScroll';
import VueApp from '../learning/maps-task/app';
import ScrollAura from '../learning/aura-test/scrollAura';
import DataVue from "../learning/data-processing/data-processing";
import ColorMap from "../learning/color-map/color-map";
import ProgramExplorer from "../learning/program-explorer/program-explorer";
import { programExplorerVueApp } from "./vue-app";
const proximityCanvasElement = document.getElementById('myCanvasProximity') as HTMLElement;
if (proximityCanvasElement) new Proximity(proximityCanvasElement);


const cursorCanvasElement = document.getElementById('myCanvasCursor') as HTMLElement;
if (cursorCanvasElement) new MagicCursorBoard(cursorCanvasElement);


const menuContainer = document.querySelector('.menu-container') as HTMLElement;
const menuTrigger = document.querySelector('.main-button') as HTMLElement;
if (menuContainer && menuTrigger) new MenuApp(menuContainer, menuTrigger);


const scrollContainer = document.getElementById('scroll-container');
const scrollElement = document.querySelectorAll('.pentagon-element');
const scrollSections = document.querySelectorAll('.section'); 
if (scrollContainer && scrollElement.length && scrollSections.length) new RobertosScroll(scrollContainer, scrollElement, scrollSections);


const vueElements = document.querySelectorAll('.vue-app') as NodeList;
const mapElement = document.getElementById('map') as HTMLElement;
if (vueElements.length) for (let i = 0; i < vueElements.length; i++) {new VueApp(vueElements[i])}


const scrollAuraElement = document.querySelector('.aura-element') as HTMLElement;
const sectionElements = document.querySelectorAll('.container') as NodeListOf<HTMLElement>;
if (scrollAuraElement && sectionElements) new ScrollAura(scrollAuraElement, sectionElements);


const dataVueAppElement = document.querySelector('.data-vue-app') as HTMLElement;
if (dataVueAppElement) new DataVue(dataVueAppElement);


const colorMapVueAppElement = document.querySelector('.color__vue-app') as HTMLElement;
if (colorMapVueAppElement) new ColorMap(colorMapVueAppElement);


const animationVueElement = document.querySelector('.animation-vue-app') as HTMLElement;
if (animationVueElement) new AnimationApp(animationVueElement);


const programExplorerElement = document.querySelector('.program-explorer') as HTMLElement;
const programExplorerVue = document.querySelector('#vue-app') as HTMLElement;
if (programExplorerElement) new ProgramExplorer(programExplorerElement);
programExplorerVueApp;


const documentElement: HTMLElement = document.getElementsByTagName('html')[0];
const screenWidth: number = window.innerWidth;
const screenHeight: number = documentElement.clientHeight;
let scrollPosition: number = 0;
const circleElement: HTMLElement = document.querySelector('.pointer-circle');
const pointer: HTMLElement = document.querySelector('.pointer');
function addEvents() {
    window.addEventListener('scroll', event => {
        scrollPosition = window.scrollY;
        if (scrollPosition === undefined) {
            scrollPosition = document.documentElement.scrollTop;
        }
    })
    window.addEventListener('mousemove', event => {
        followPointer(event);
    })
}
function followPointer(e) {
    const rawX = e.clientX;
    const rawY = e.clientY;
    const halfW = screenWidth / 2;
    const halfH = (screenHeight / 2) - scrollPosition;
    gsap.to(circleElement, 0.6, {
        x: rawX - halfW,
        y: rawY - halfH, ease: Power1.easeInOut
    });
    gsap.to(pointer, 0.05, {
        x: rawX - halfW,
        y: rawY - halfH
    });
}
//addEvents();

function getFocuseable(focusNode, rootNode = document) {
    let focusableElements = Array.from(document.querySelectorAll('a, button, input, input[type=checkbox]'));
    for (let i = 0; i < focusableElements.length; i++) {
        const element = focusableElements[i];
        element.addEventListener('mouseover', function() {
            circleElement.classList.add('hovering');
            pointer.classList.add('hovering');
        });
        element.addEventListener('mouseleave', function() {
            circleElement.classList.remove('hovering');
            pointer.classList.remove('hovering');
        });
    }
}

setTimeout(() => {
    //getFocuseable(documentElement)
}, 500)
