<?php /* Template Name: Home */ 

$context = Timber::context();
$posts = new TimberPost();
$context['posts'] = $posts;
$context['consolas'] = new Timber\Term('consoles');
$projects = array(
    'post_type'         => 'projects',
    'post_status'       => 'publish',
    'tax_query'         => array(
        array(
            'taxonomy'  => 'types',
            'field'     => 'slug',
            'terms'     => 'project',
        )
    )
);

Timber::render( 'homepage.twig', $context);
?>