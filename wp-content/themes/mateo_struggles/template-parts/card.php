<?php
/**
* Template Name: Card
*
* @package mateo_struggles
*/
?>
<?php while ( have_rows('card') ) : the_row(); ?>			   
    // display a sub field value
    <div class="card"></div>
    <img src=<?php the_sub_field('card_image') ?>>
    <h1><?php $card_title ?></h1>
    <p><?php $card_text ?></p>
<?php endwhile; ?>