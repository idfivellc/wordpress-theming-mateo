<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mateo_struggles
 */

//get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php 
		if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); 
				the_title( '<h1>', '</h1>' ); 
				the_post_thumbnail(); 
				the_excerpt( '<p>', '</p>' ); ?>
				<?php if( have_rows('builder_content') ):

					// Loop through rows.
					while ( have_rows('builder_content') ) : the_row(); ?>
				
						// Case: Paragraph layout.
						<?php if( get_row_layout() == 'card' ): ?>
							<?php include 'template-parts/card.php';?>
							// Do something...
						<?php elseif( get_row_layout() == 'content_text'):
							the_sub_field('text');
						endif; ?>
				
					<?php endwhile; ?>
				
				<?php else :
					// Do something...
				endif;
			   ?>
			<?php endwhile; ?>
			<?php else: 
			_e( 'Sorry, no posts matched your criteria.', 'textdomain' ); 
		endif; 
?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
/* get_sidebar(); */
get_footer();
